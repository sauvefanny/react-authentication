import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const userSlice = createSlice({
    name: 'user',
    initialState: {
        list: []
    },
    reducers: {
        setList(state, { payload }) {
            state.list = payload;
        },
        addUser(state, { payload }) {
            state.list.push(payload);
        }
    }
});
export const{setList, addUser} = userSlice.actions;

export default userSlice.reducer;

export const fetchUsers = (list) => async (dispatch) => {
    
    const response = await axios.get ('http://localhost:8000/api/user');

    dispatch(setList(response.data))
        
}