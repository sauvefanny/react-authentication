import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./auth-slice";
import userSlice from "./user-slice";

/**
 * Le store redux va contenir les différentes slices qu'on aura
 * définies avec le "prefix" auxquel on les assigne.
 * Ici on dit que tout le state de notre authSlice sera accessible
 * à partir de `auth` donc si on veut accéder à l'objet user du
 * state de authslice, il faudra faire state.auth.user depuis
 * un component
 */

export const store = configureStore({
    reducer: {
        auth: authSlice,
        user: userSlice

    }
});

