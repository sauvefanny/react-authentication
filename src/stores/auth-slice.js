import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";
import { addUser } from "./user-slice";


/**
 * Une slice représente un state global associé à une feature,
 * ici on fait une slice pour gérer le state lié à l'authentification.
 * La seule manière de muter/modifier la valeur du state d'une slice
 * se fera en passant par des reducers (ou actions) qu'on définies
 * dans la slice en question. (toutes les modifications de state devront
 * se faire à l'intérieur d'une de ces actions)
 */
const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        loginFeedback: '',
        registerFeedback: ''
    },
    reducers: {
        login(state, {payload}) {
            state.user = payload;
        },
        logout(state) {
            state.user = null;
            localStorage.removeItem('token');
        },
        updateLoginFeedback(state, {payload}) {
            state.loginFeedback = payload
        },
        updateRegisterFeedback(state, {payload}) {
            state.registerFeedback = payload
        }
    }
});
/**
 * On exporte les différentes actions afin de pouvoir s'en servir
 * facilement depuis les components
 */
export const {login,logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;

//On export le reducer pour la charger dans le store
export default authSlice.reducer;


export const loginWithToken = () => async (dispatch) => {
    const token = localStorage.getItem('token');

    if(token) {
        try {

            const user = await AuthService.fetchAccount();

            dispatch(login(user));
        } catch(error) {
            dispatch(logout());
        }
    }
    
}

export const register = (user) => async (dispatch) => {
    try {
        await AuthService.register(user);
        dispatch(updateRegisterFeedback('Registration successful'));
        dispatch(addUser(user));
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
        
    }
}

export const loginWithCredentials = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback('Login successful'));
        dispatch(login(user));

    } catch (error) {
        dispatch(updateLoginFeedback('Email and/or password incorrect'));
    }
}