

import { Layout } from "antd";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { Nav } from "./components/Nav";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Register } from "./pages/Register";
import { loginWithToken } from "./stores/auth-slice";

const { Header, Content, Footer } = Layout;

function App() {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(loginWithToken());
  }, []);


  return (
    <Layout>
      <Header>
        <Nav />
      </Header>
      <Content>
        <Switch>
          <Route path="/register">
            <Register />
          </Route>

          <Route path="/login">
            <Login />
          </Route>

          <Route path="/" exact>
            <Home />

          </Route>

        </Switch>
      </Content>
      <Footer>
        <p>Copyleft no one</p>
      </Footer>
    </Layout>
  );
}

export default App;
